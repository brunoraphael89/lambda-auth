var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://wallyet-f3838.firebaseio.com"
});

exports.handler = (event, context, callback) => {
    // TODO implement
    var idToken = event.authorizationToken


    admin.auth().verifyIdToken(idToken)
      .then(function(decodedToken) {
        callback(null, generatePolicy('user', 'Allow', event.methodArn))

      }).catch(function(error) {
        context.done("error",'Invalid idToken' + e.message)
        callback(null, generatePolicy('user', 'Deny', event.methodArn))
      })

};


var generatePolicy = function(principalId, effect, resource) {
    var authResponse = {};

    authResponse.principalId = principalId;
    if (effect && resource) {
        var policyDocument = {};
        policyDocument.Version = '2012-10-17';
        policyDocument.Statement = [];
        var statementOne = {};
        statementOne.Action = 'execute-api:Invoke';
        statementOne.Effect = effect;
        statementOne.Resource = resource;
        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument;
    }

    // Optional output with custom properties of the String, Number or Boolean type.
    authResponse.context = {
        "stringKey": "stringval",
        "numberKey": 123,
        "booleanKey": true
    };
    return authResponse;
}
